/*
 * Autor       : Mikael Nilsson
 * Filename    : MyComplex.h
 * Description : Simple interface for a complexnumber
 *
*/

#ifndef MYCOMPLEX_H
#define MYCOMPLEX_H

#include <string>
#include <ostream>

class MyComplex
{
public:
    MyComplex(double real=0.0, double imaginary=0.0);
    double abs() const;
    bool operator==(const MyComplex& myComplex) const;
    MyComplex operator+(const MyComplex& myComplex) const;
    MyComplex operator-(const MyComplex& myComplex) const;
    MyComplex operator*(const MyComplex& myComplex) const;
    MyComplex operator/(const MyComplex& myComplex) const;
    MyComplex operator+(double real) const;
    MyComplex operator-(double real) const;
    MyComplex operator*(double real) const;
    MyComplex operator/(double real) const;
    double real() const;
    double imaginary() const;
private:
    double m_real;
    double m_imaginary;
};

std::ostream& operator<<(std::ostream& os, const MyComplex& myComplex);

#endif // MYCOMPLEX_H
