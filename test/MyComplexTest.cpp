/*
 * Autor       : Mikael Nilsson
 * Filename    : MyComplexTest.cpp
 * Description : Enhetstester för att testa MyComplex olika medlemsfunktioner
 *
*/

#include <catch.hpp>
#include "MyComplex.h"

TEST_CASE("MyComplex TestCase") {
    MyComplex complex(2.0, -1.0);

    SECTION("Test complex addition") {
        MyComplex complexOperation(complex +  MyComplex(1.0, -1.0));
        REQUIRE(complexOperation.real() == Approx(3.0));
        REQUIRE(complexOperation.imaginary() == Approx(-2.0));
    }

    SECTION("Test scalar addition") {
        MyComplex complexOperation(complex + 5);
        REQUIRE(complexOperation.real() == Approx(7.0));
        REQUIRE(complexOperation.imaginary() == Approx(-1.0));
    }

    SECTION("Test complex subtraction") {
        MyComplex complexOperation(complex - MyComplex(1.0, -1.0));
        REQUIRE(complexOperation.real() == Approx(1.0));
        REQUIRE(complexOperation.imaginary() == Approx(0.0));
    }

    SECTION("Test scalar subtraction") {
        MyComplex complexOperation(complex - 5);
        REQUIRE(complexOperation.real() == Approx(-3.0));
        REQUIRE(complexOperation.imaginary() == Approx(-1.0));
    }

    SECTION("Test complex multiplicaton") {
        MyComplex complexOperation(complex * MyComplex(1.0, -1.0));
        REQUIRE(complexOperation.real() == Approx(1.0));
        REQUIRE(complexOperation.imaginary() == Approx(-3.0));
    }

    SECTION("Test scalar multiplication") {
        MyComplex complexOperation(complex * 5);
        REQUIRE(complexOperation.real() == Approx(10.0));
        REQUIRE(complexOperation.imaginary() == Approx(-5.0));
    }

    SECTION("Test complex division") {
        MyComplex complexOperation(complex / MyComplex(1.0, -1.0));
        REQUIRE(complexOperation.real() == Approx(1.5));
        REQUIRE(complexOperation.imaginary() == Approx(0.5));
    }

    SECTION("Test scalar division") {
        MyComplex complexOperation(complex / 2);
        REQUIRE(complexOperation.real() == Approx(1.0));
        REQUIRE(complexOperation.imaginary() == Approx(-0.5));
    }

    SECTION("Test absolute value") {
        REQUIRE(complex.abs() == Approx(sqrt(5.0)));
        MyComplex complex2(4.0, 3.0);
        REQUIRE(complex2.abs() == Approx(5.0));
    }

    SECTION("Test real value") {
        REQUIRE(complex.real() == Approx(2.0));
    }

    SECTION("Test imaginary value") {
        REQUIRE(complex.imaginary() == Approx(-1.0));
    }
}
