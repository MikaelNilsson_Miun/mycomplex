/*
 * Autor       : Mikael Nilsson
 * Filename    : MyComplex.cpp
 * Description : Simple implementation for a complexnumber
 *
*/

#include "MyComplex.h"

#include <cmath>
#include <iostream>
#include <string>

MyComplex::MyComplex(double real, double imaginary)
    : m_real(real), m_imaginary(imaginary)
{}

double MyComplex::abs() const
{
    return sqrt(m_real*m_real + m_imaginary*m_imaginary);
}

bool MyComplex::operator==(const MyComplex &myComplex) const
{
    return (m_real == myComplex.m_real && m_imaginary == myComplex.m_imaginary);
}

MyComplex MyComplex::operator+(const MyComplex &myComplex) const
{
    return MyComplex(m_real + myComplex.m_real, m_imaginary + myComplex.m_imaginary);
}

MyComplex MyComplex::operator-(const MyComplex &myComplex) const
{
    return MyComplex(m_real - myComplex.m_real, m_imaginary - myComplex.m_imaginary);
}

MyComplex MyComplex::operator*(const MyComplex &myComplex) const
{
    return MyComplex(
                m_real * myComplex.m_real - m_imaginary * myComplex.m_imaginary,
                m_imaginary * myComplex.m_real + m_real * myComplex.m_imaginary
                );
}

MyComplex MyComplex::operator/(const MyComplex &myComplex) const
{
    double divisor = myComplex.m_real*myComplex.m_real + myComplex.m_imaginary*myComplex.m_imaginary;
    return MyComplex(
                (m_real * myComplex.m_real + m_imaginary * myComplex.m_imaginary)/divisor,
                (m_imaginary * myComplex.m_real - m_real * myComplex.m_imaginary)/divisor
                );
}

MyComplex MyComplex::operator+(double real) const
{
    return MyComplex(m_real + real, m_imaginary);
}

MyComplex MyComplex::operator-(double real) const
{
    return MyComplex(m_real - real, m_imaginary);
}

MyComplex MyComplex::operator*(double real) const
{
    return MyComplex(m_real * real, m_imaginary * real);
}

MyComplex MyComplex::operator/(double real) const
{
    return MyComplex(m_real / real, m_imaginary / real);
}

double MyComplex::real() const
{
    return m_real;
}

double MyComplex::imaginary() const
{
    return m_imaginary;
}

std::ostream &operator<<(std::ostream &os, const MyComplex &myComplex)
{
    return os << myComplex.real()
              << (myComplex.imaginary() >= 0 ? " + " : " - ")
              << std::abs(myComplex.imaginary()) << "i";
}
