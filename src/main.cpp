/*
 * Autor       : Mikael Nilsson
 * Filename    : main.cpp
 * Description : Prints out complex number calculations
 *
*/

#include <iostream>
#include "MyComplex.h"

using namespace std;

int main(int argc, char *argv[])
{
    MyComplex complex(10, -2);
    MyComplex complex2(-1.5, 3.4);

    char ops[] = {'+', '-', '*', '/'};
    MyComplex awnser[] = {complex + complex2, complex - complex2, complex * complex2, complex / complex2};

    for(int i = 0; i < 4; i++)
    {
        cout << '(' << complex << ") " << ops[i] << " (" << complex2 << ") = " << awnser[i] << endl;
    }

    return 0;
}







